What Is This?
-------------

The UserBallot module helps you add UserBallot (https://www.userballot.com/)
user feedback to a Drupal site. UserBallot is a simple decision support tool
that allows online businesses to collect directional feedback from their site
visitors and use it to make better business decisions. Installing UserBallot
on your site will allow you to easily deploy yes or no questions to your site
visitors and immediately start seeing the results of their feedback.

Are yes or no questions a good idea? 
Yes! By creating short yes or no questions that only require one touch to
answer, your site visitors are more likely to answer them. This gives you
the information you need to make better decisions!

Features:

- Create simple yes or no feedback questions based on UserBallot's suggestions,
or your own ideas.
- Set the frequency at which questions are served to individual site visitors.
- See responses to questions in real time!
- Gain direction from your customers before spending a lot of time creating and
delivering products and services to them.
- Understand more about the users answering your questions thanks to analytics.
- Get upcoming UserBallot features without upgrading the module.


Requirements
------------

Drupal 7.x
No other module dependencies.


Installation
------------

1. Install the module as usual using the standard Drupal installation process
2. If you haven't already you will need to sign up for a UserBallot account
3. Get your UserBallot site ID (found in your UserBallot account by clicking
"Get Started" and the CMS tab)
4. Go to the UserBallot configuration page in your Drupal site
(admin/config/content/userballot), and save your site ID.
5. Start getting feedback from your users


Additional Notes
----------------

Almost all configuration is done from within your UserBallot account. However,
you do have the local option to show or hide the UserBallot feedback when in
admin pages.
